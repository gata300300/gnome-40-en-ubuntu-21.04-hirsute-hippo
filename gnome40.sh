#!/bin/bash
echo
echo ¡Hola!
echo
echo Este es el script de Roberto Díaz
echo
echo Para instalar gnome 40 en ubuntu 21.04
echo
echo Vamos!
echo
slepp 3s
sudo add-apt-repository ppa:shemgp/gnome-40
sudo apt update && sudo apt full-upgrade
echo
sudo apt install gnome-session adwaita-icon-theme-full fonts-cantarell

#echo Ahora voy a instalarte la última versión de yaru thume por si quieres mantener este tema tan conocido que usa ubuntu por defecto
#sudo apt install git meson sassc libglib2.0-dev libxml2-utils
#git clone https://github.com/ubuntu/yaru
#cd yaru
#meson build
#sudo ninja -C build install
#
#echo "Ahora voy a eliminar el directorio yaru de tu home, para que no dejarte cositas por ahí ;)"
#sudo rm -r yaru

echo
echo Pues ya esta!
sleep 2s
echo
echo
echo "Ahora debes reiniciar el sistema para que los cambios se apliquen, después inicia sesión con gnome o gnome en xorg (engranaje de abajo a la derecha en la pantalla de inicio de sesión)"
echo
    read -p "¿Quieres reiniciar el sistema (s/n)?" sn
    case $sn in
        [Ss]* ) sudo reboot;;
        [Nn]* ) exit;;
        * ) echo "Por favor, pulsa s o n.";;
    esac
done